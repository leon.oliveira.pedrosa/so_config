from django.db.utils import IntegrityError
from rest_framework import serializers
from rest_framework.exceptions import APIException
from api.models import *


class CameraSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    alias = serializers.CharField(max_length=255)

    def create(self):
        pass

    def update(self):
        pass


class NotifySerializer(serializers.Serializer):
    cam = serializers.UUIDField()
    user = serializers.CharField()
    notify = serializers.BooleanField()
    range = serializers.JSONField()

    def create(self, validated_data):
        try:
            c = CameraModel.objects.get(id=validated_data['cam'])
            u = User.objects.get(id=validated_data['user'])

            new_notify = NotifyModel.objects.create(
                cam=c,
                user=u,
                notify=validated_data['notify'],
                range=validated_data['range']
            )
        except CameraModel.DoesNotExist:
            raise APIException('Camera não existe')
        except User.DoesNotExist:
            raise APIException('User não existe')
        except IntegrityError as error:
            raise APIException(error)



        return new_notify

    def update(self):
        pass

