from django.shortcuts import render
from django.utils.timezone import make_aware

from rest_framework.viewsets import ViewSet, GenericViewSet
from rest_framework.mixins import *
from rest_framework.response import Response
from rest_framework.decorators import action

from api.serializer import *

from datetime import datetime


def verify_time(range):
    now = datetime.strptime(make_aware(datetime.now().time()).strftime("%H:%M:%S"), "%H:%M:%S").time()
    for time in range:
        start = datetime.strptime(time['start'], "%H:%M:%S").time()
        end = datetime.strptime(time['end'], "%H:%M:%S").time()
        if now > start and now < end:
            return True
    return False


class NotifyViewSet(ListModelMixin, CreateModelMixin, GenericViewSet):

    serializer_class = NotifySerializer


class SendNotify(ViewSet):
    @action(methods=['GET'], detail=True, url_path='abc')
    def send(self, request, *args, **kwargs):
        cam = CameraModel.objects.get(id=kwargs['pk'])
        user = User.objects.get(id=1)
        n = NotifyModel.objects.get(cam=cam, user=user)
        if n.notify:
            day_w = datetime.now().isoweekday()
            range_hours = n.range[str(day_w)]
            if verify_time(range_hours):
                return Response('Enviando notificação')
            else:
                return Response('Não envia notificação')
