from rest_framework import routers
from unicodedata import name
from api.views import *


router = routers.DefaultRouter()

router.register(r'notify', NotifyViewSet, basename='notify')
router.register(r'send', SendNotify, basename='send')

urlpatterns = router.urls
