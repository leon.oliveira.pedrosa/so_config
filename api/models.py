import uuid
from django.db import models
from django.contrib.auth.models import User


class CameraModel(models.Model):
     id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
     alias = models.CharField(max_length=255)

     def __str__(self):
         return self.alias


class NotifyModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    cam = models.ForeignKey(CameraModel, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notify = models.BooleanField(default=True)
    range = models.JSONField()

#    def __str__(self):
#        return self.id

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['cam', 'user'], name='cam and user')
        ]
