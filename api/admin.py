from django.contrib import admin
from api.models import *


class NotifyAdmin(admin.ModelAdmin):
    list_display = ('cam', 'user', 'notify', 'range')


class CamAdmin(admin.ModelAdmin):
    list_display = ('alias', )


admin.site.register(NotifyModel, NotifyAdmin)
admin.site.register(CameraModel, CamAdmin)
